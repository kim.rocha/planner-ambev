
import { config, clients } from './input'







//NÃO ALTERAR NADA A PARTIR DESSA LINHA

export const getClients = () => {
    return clients.map((client) => ({
        cod: client.cod,
        lat: Number(client.lat.replace(',', '.')) / (config.measureUnit === 'KM' ? 111.32 : 1),
        long: Number(client.long.replace(',', '.')) / (config.measureUnit === 'KM' ? 111.32 : 1)
    }))
}

export { config };
