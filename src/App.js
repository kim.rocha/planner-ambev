import React from 'react';
import './App.css';
import { config, getClients } from './clients.js'
import kmeans from 'node-kmeans'
import fileDownload from 'js-file-download'
import kmeanslib from 'kmeans-same-size'
import Papa from 'papaparse'




function distanceToLatLong(a, b, unit = 'K') {


  const lat1 = a[0]
  const lon1 = a[1]
  const lat2 = b[0]
  const lon2 = b[1]

  if ((lat1 === lat2) && (lon1 === lon2)) {
    return 0;
  }
  else {
    var radlat1 = Math.PI * lat1 / 180;
    var radlat2 = Math.PI * lat2 / 180;
    var theta = lon1 - lon2;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit === "K") { dist = dist * 1.609344 }
    if (unit === "N") { dist = dist * 0.8684 }
    return dist;
  }
}



function App() {



  const clients = getClients();




  if (config.sameSize) {

    let vec2 = clients.map((client, i) => ({ cod: client.cod, x: client.lat, y: client.long }))
    const sskmeans = new kmeanslib()
    sskmeans.init({ k: config.numberOfClusters, runs: parseInt(clients.length / 6), equalSize: true, normalize: false })
    sskmeans.calc(vec2);
    fileDownload(Papa.unparse(vec2.map((point) => ({
      cod: point.cod,
      k: point.k,
      lat: point.x,
      long: point.y
    }))), `same-sized-clusters.csv`)

  } else {

    let clusterizedClients = [];

    let vec = clients.map((client) => ([client.lat, client.long]))

    kmeans.clusterize(vec, {
      k: config.numberOfClusters,
      distance: distanceToLatLong
    }, (err, res) => {
      if (err) console.error(err);
      else {
        clusterizedClients = (res.map((cluster, i) => (
          cluster.cluster.map((point, j) => ({
            cod: clients[cluster.clusterInd[j]].cod,
            k: i,
            lat: point[0],
            long: point[1]
          }))
        )))
        console.log(res)
        fileDownload(Papa.unparse([].concat.apply([], clusterizedClients)), `diff-sized-cluster.csv`)
      }
    })
  }


  return (
    <div className="App">
      <header className="App-header">
        <span>Rodou!</span>
      </header>
    </div>
  );
}

export default App;
